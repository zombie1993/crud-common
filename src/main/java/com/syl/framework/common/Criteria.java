package com.syl.framework.common;

/**
 * 简单封装的查询条件工具类  辅助实现分页,查询条件,排序
 * 待扩展
 *
 * @author syl
 * @create 2018-06-15 10:47
 **/
public class Criteria {
    private static final String ESCAPES = "`";
    private StringBuffer queryColumn = new StringBuffer();
    private StringBuffer orderColumn = new StringBuffer();
    private StringBuffer groupColumn  = new StringBuffer();

    /**
     * 是否 distinct
     */
    private boolean distinct;

    /**
     * 是否模糊查询  模糊条件默认为全匹配 `%xxx%`
     */
    private boolean like;

    /**
     * 查询条件
     */
    private String query;

    /**
     * 排序
     */
    private String orderBy;

    /**
     * 是否逆序
     */
    private boolean reverse;

    /**
     * 批量操作所依据的 key
     */
    private String whereInKey;

    /**
     * 当前页
     */
    private Integer pageCur;
    /**
     * 每页几行
     */
    private Integer pageRow;

    //====无需关心参数start=====
    /**
     * 实际分页偏移
     */
    private Integer offset;

    /**
     * 实际行数
     */
    private Integer row;

    //====无需关心参数end=====
    /**
     * simple
     */
    public Criteria() {
        this(null,null);
    }

    /**
     * 批量操作所依据的 where key in
     * @param whereInKey
     */
    public Criteria(String whereInKey){
        this(whereInKey,null,null);
    }

    /**
     * 设置默认数据库类型
     * @param pageCur 当前页
     * @param pageRow 每页几行
     */
    public Criteria(Integer pageCur, Integer pageRow) {
        this(null,pageCur,pageRow);
    }

    /**
     * 设置批量操作所依据的 where key in 并使用分页
     * @param whereInKey
     * @param pageCur
     * @param pageRow
     */
    public Criteria(String whereInKey, Integer pageCur, Integer pageRow) {
        if(whereInKey != null) {
            this.whereInKey = ESCAPES + whereInKey + ESCAPES;
        }
        if(whereInKey != null && whereInKey.isEmpty()){
            throw new RuntimeException("whereInKey 不能为空字符串");
        }
        this.pageCur = pageCur;
        this.pageRow = pageRow;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public Criteria setDistinct(boolean distinct) {
        this.distinct = distinct;
        return this;
    }

    /** 添加查询字段
     * @param column
     * @return
     */
    public Criteria addQuery(String column) {
        return getColumn(column, queryColumn);
    }

    public String getQuery() {
        String q = queryColumn.toString();
        if(q.trim().isEmpty())return " * ";
        this.query = q.substring(0, q.length()-1);//因设置时全部往后累加一个字符
        return query;
    }

    public String getOrderBy() {
        String q = orderColumn.toString();
        if(q.trim().isEmpty())return null;
        this.orderBy = q.substring(0, q.length()-1);//因设置时全部往后累加一个字符
        return isReverse() ? orderBy+" desc " : orderBy;
    }

    /** 添加order by字段
     * @param column
     * @return
     */
    public Criteria addOrderby(String column) {
        return getColumn(column, orderColumn);
    }

    public boolean isReverse() {
        return reverse;
    }

    /** 是否逆序
     * @param reverse
     */
    public Criteria setReverse(boolean reverse) {
        this.reverse = reverse;
        return this;
    }

    /**
     *  获取当前页
     * @return
     */
    public Integer getPageCur() {
        return pageCur;
    }

    /**
     *  获取每页显示几行
     * @return
     */
    public Integer getPageRow() {
        return pageRow;
    }

    /**
     * 是否模糊查询
     * @return
     */
    public boolean isLike() {
        return like;
    }

    public Criteria setLike(boolean like) {
        this.like = like;
        return this;
    }

    public String getWhereInKey() {
        return whereInKey;
    }

    /**
     *
     * @param offset
     * @return
     */
    public Criteria setOffset(Integer offset) {
        this.offset = offset;
        return this;
    }

    public Criteria setRow(Integer row) {
        this.row = row;
        return this;
    }

    public Integer getOffset() {
        return offset;
    }

    public Integer getRow() {
        return row;
    }

    public static String likeFormat(String str){
        return "%"+str+"%";
    }

    private Criteria getColumn(String column, StringBuffer stringBuffer) {
        if(column == null || column.isEmpty())return this;
        stringBuffer.append(" ");
        stringBuffer.append(ESCAPES);
        stringBuffer.append(column);
        stringBuffer.append(ESCAPES);
        stringBuffer.append(",");
        return this;
    }

    @Override
    public String toString() {
        return "query :"+getQuery()+"whereKey :"+getWhereInKey() +" like :"+isLike()+" orderBy :"+getOrderBy()+" page limit :"+getPageCur()+","+getPageRow();
    }

}
