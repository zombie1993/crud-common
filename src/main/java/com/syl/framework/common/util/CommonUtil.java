package com.syl.framework.common.util;

import java.util.List;

/**
 * @author syl
 * @create 2018-06-18 18:30
 **/
public class CommonUtil {

    //list: 要拆分的列表
    //pageSize: 每个子列表条数
    public static List[] splitList(List list, int pageSize) {
        int total = list.size();
        //总页数
        int pageCount = total % pageSize == 0 ? total / pageSize : total / pageSize + 1;
        List[] result = new List[pageCount];
        for(int i = 0; i < pageCount; i++) {
            int start = i * pageSize;
            //最后一条可能超出总数
            int end = start + pageSize > total ? total : start + pageSize;
            List subList = list.subList(start, end);
            result[i] = subList;
        }
        return result;
    }

}
