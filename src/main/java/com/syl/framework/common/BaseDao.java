package com.syl.framework.common;

import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 基础dao
 *
 * @author syl
 * @create 2018-06-15
 **/
public interface BaseDao<Bean,DTO> {

    /**
     *  选择性插入一条记录
     *  @param bean bean
     */
    long insertSelective(Bean bean);//插入不能使用Param，适配自增长时可返回id

    /**
     *  批量插入数据
     * @param beanList beanList
     * @return
     */
    long insertSelectiveList(List<Bean> beanList);

    /**
     *  根据id删除记录
     *  @param id id
     */
    int deleteById(@Param("id") String id);

    /**
     *  根据id批量删除数据
     *  @param idList idList
     */
    int deleteByIdList(@Param("valueList") List<String> idList);

    /**
     *  根据指定列删除记录
     *  @param criteria 支持whereInKey参数 必需
     *  @param columnValue columnValue
     */
    int deleteByColumn(@Param("criteria") Criteria criteria, @Param("columnValue") String columnValue);

    /**
     *  根据指定列批量删除数据
     *  @param criteria 支持whereInKey参数 必需
     *  @param valueList valueList
     */
    int deleteByColumnList(@Param("criteria") Criteria criteria, @Param("valueList") List<String> valueList);

    /**
     *  根据id更新记录
     *  @param bean set value bean
     *  @param id   id value
     */
    int updateById(@Param("bean") Bean bean,@Param("id") String id);

    /**
     *  根据id列表批量更新数据
     *  仅限更新值为一样的情况
     *  @param bean set value bean
     *  @param idList idList
     */
    int updateByIdList(@Param("bean") Bean bean, @Param("valueList") List<String> idList);

    /**
     *  根据指定列更新记录
     *  @param criteria 支持whereInKey参数 必需
     *  @param bean set value bean
     *  @param columnValue columnValue
     */
    int updateByColumn(@Param("criteria") Criteria criteria, @Param("bean") Bean bean,@Param("columnValue") String columnValue);

    /**
     *  根据指定列批量更新数据
     *  仅限更新值为一样的情况
     *
     *  @param criteria 支持whereInKey参数 必需
     *  @param bean set value bean
     *  @param valueList valueList
     */
    int updateByColumnList(@Param("criteria") Criteria criteria, @Param("bean") Bean bean, @Param("valueList") List<String> valueList);

    /**
     *  查询所有记录
     */
    List<Bean> selectAll();

    /**
     *  依据条件查询是否存在记录
     *  @param criteria criteria 只支持like参数
     *  @param bean bean
     */
    boolean isExist(@Param("criteria") Criteria criteria, @Param("bean") DTO bean);

    /**
     *  根据id查询记录
     *  @param criteria criteria 只支持query参数
     *  @param id id
     */
    Bean selectById(@Param("criteria") Criteria criteria,@Param("id") String id);

    /**
     * 根据id列表查询记录
     * @param criteria criteria 只支持query参数
     * @param idList idList
     * @return
     */
    List<Bean> selectByIdList(@Param("criteria") Criteria criteria,@Param("valueList") List<String> idList);

    /**
     *  根据指定列查询记录
     *  @param criteria criteria 只支持whereInKey,
     *  @param columnValue columnValue
     */
    List<Bean> selectByColumn(@Param("criteria") Criteria criteria, @Param("columnValue") String columnValue);

    /**
     * 根据指定列查询记录列表
     * @param criteria need whereInKey constructor method
     * @param valueList valueList
     * @return
     */
    List<Bean> selectByColumnList(@Param("criteria") Criteria criteria, @Param("valueList") List<String> valueList);

    /**
     *  依据条件查询一条记录
     *  @param criteria criteria 支持所有参数
     *  @param bean bean
     */
    Bean selectOne(@Param("criteria") Criteria criteria, @Param("bean") DTO bean);

    /**
     *  依据条件查询记录列表
     *  @param criteria criteria 支持所有参数
     *  @param bean bean
     */
    List<Bean> selectList(@Param("criteria") Criteria criteria, @Param("bean") DTO bean);

    /**
     *  依据条件查询记录数量
     *  @param criteria criteria 支持所有参数
     *  @param bean bean
     */
    long selectListCount(@Param("criteria") Criteria criteria, @Param("bean") DTO bean);

}
