package com.syl.framework.common;

import com.syl.framework.common.annotations.PrimaryKey;
import com.syl.framework.common.enums.PrimaryKeyTypeEnum;
import com.syl.framework.common.util.CommonUtil;
import com.syl.framework.common.util.ReflectUtil;
import org.apache.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * 基础service 实现类
 *
 * @author syl
 * @create 2018-03-17 22:10
 **/
public class BaseServiceImpl<Bean,DTO> implements BaseService<Bean,DTO>{
    private static final Logger LOG = Logger.getLogger(BaseServiceImpl.class);
    private static final int MAX_BATCH_COUNT = 1000;
    private cn.ms.sequence.Sequence sequence = new cn.ms.sequence.Sequence(0, 0);

    protected BaseDao<Bean,DTO> dao;

    /**
     * 需要注入实际dao对象
     * @param dao
     */
    public void setDao(BaseDao<Bean,DTO> dao) {
        this.dao = dao;
    }

    @Override
    public String save(Bean bean) {
        int type = getAnnotationType(bean);
        setPrimaryId(bean,type);
        setFieldValueHelp(bean,"delete", false);
        setFieldValueHelp(bean,"createTime",new Date());
        LOG.debug("待保存数据====》"+bean);
        long l = dao.insertSelective(bean);
        String id = getFieldValueHelp(bean, "id", String.class);
        LOG.debug("插入了====》"+l+"条 id为====》"+id);
        return id;
    }

    @Override
    public List<String> saveList(List<Bean> beanList) {
        List<String> resId = new ArrayList<>();
        if(beanList.size() <= 0)return resId;
        if(beanList.size() <= MAX_BATCH_COUNT){
            for (Bean bean : beanList) {
                int type = getAnnotationType(bean);
                setPrimaryId(bean,type);
                setFieldValueHelp(bean,"delete", false);
                setFieldValueHelp(bean,"createTime",new Date());
            }
            long l = dao.insertSelectiveList(beanList);
            LOG.debug("插入了====》"+l+"条");
            for (Bean bean : beanList) {
                resId.add(getFieldValueHelp(bean,"id",String.class));
            }
        }else {
            List<Bean>[] lists = CommonUtil.splitList(beanList, MAX_BATCH_COUNT);
            LOG.debug("saveList 拆分出====》"+lists.length+"个任务数");
            for (List<Bean> list : lists) {
                resId.addAll(this.saveList(list));
            }
        }
        LOG.debug("批量插入了====》"+resId.size()+"条 共需要插入数====》 "+beanList.size());
        return resId;
    }

    @Override
    public int deleteById(String id) {
        int i = dao.deleteById(id);
        LOG.debug("删除了====》"+i+"条 id为====》 "+id);
        return i;
    }

    @Override
    public int deleteByIdList(List<String> idList) {
        int res = 0;
        if(idList.size() <= 0) return res;
        if(idList.size() <= MAX_BATCH_COUNT){
            res = dao.deleteByIdList(idList);
        }else {
            List<String>[] lists = CommonUtil.splitList(idList, MAX_BATCH_COUNT);
            LOG.debug("批量删除 拆分出====》"+lists.length+"个任务数");
            for (List<String> list : lists) {
                int i = this.deleteByIdList(list);
                res = res + i;
            }
        }
        LOG.debug("批量删除了====》"+res+"条  共需要删除数====》"+idList.size());
        return res;
    }

    @Override
    public int deleteByColumn(Criteria criteria, String columnValue) {
        int i = dao.deleteByColumn(criteria, columnValue);
        LOG.debug("依据列删除了====》"+i+"条  依据====》"+criteria.getWhereInKey()+" 值====》"+columnValue);
        return i;
    }

    @Override
    public int deleteByColumnList(Criteria criteria, List<String> valueList) {
        int res = 0;
        if(valueList.size() <= 0) return res;
        if(valueList.size() <= MAX_BATCH_COUNT){
            res = dao.deleteByColumnList(criteria, valueList);
        }else {
            List<String>[] lists = CommonUtil.splitList(valueList, MAX_BATCH_COUNT);
            LOG.debug("依据列批量删除 拆分出====》"+lists.length+"个任务数");
            for (List<String> list : lists) {
                int i = this.deleteByColumnList(criteria,list);
                res = res + i;
            }
        }
        LOG.debug("依据列批量删除了====》"+res+"条  共需要删除数====》"+valueList.size());
        LOG.debug("列====》"+criteria.getWhereInKey()+" 值====》"+valueList);
        return res;
    }

    @Override
    public int updateById(Bean bean, String id) {
        setFieldValueHelp(bean,null,"updateTime",new Date());
        LOG.debug("待更新记录====》"+bean+" id====》"+id);
        int i = dao.updateById(bean, id);
        LOG.debug("依据id更新了====》"+i+"条记录 id====》"+id);
        return i;
    }

    @Override
    public int updateByIdList(Bean bean, List<String> idList) {
        int res = 0;
        if(idList.size() <= 0) return res;
        setFieldValueHelp(bean,null,"updateTime",new Date());
        if(idList.size() <= MAX_BATCH_COUNT){
            res = dao.updateByIdList(bean,idList);
        }else {
            List<String>[] lists = CommonUtil.splitList(idList, MAX_BATCH_COUNT);
            LOG.debug("依据id批量更新 拆分出====》"+lists.length+"个任务数");
            for (List<String> list : lists) {
                int i = this.updateByIdList(bean,list);
                res = res + i;
            }
        }
        LOG.debug("依据id批量更新了====》"+res+"条记录  idList====》"+idList);
        return res;
    }

    @Override
    public int updateByColumn(Criteria criteria, Bean bean, String columnValue) {
        setFieldValueHelp(bean,null,"updateTime",new Date());
        LOG.debug("待更新记录====》"+bean);
        int i = dao.updateByColumn(criteria, bean, columnValue);
        LOG.debug("依据列更新了====》"+i+"条记录  列====》"+criteria.getWhereInKey()+" 值====》"+columnValue);
        return i;
    }

    @Override
    public int updateByColumnList(Criteria criteria, Bean bean, List<String> valueList) {
        int res = 0;
        if(valueList.size() <= 0) return res;
        setFieldValueHelp(bean,null,"updateTime",new Date());
        if(valueList.size() <= MAX_BATCH_COUNT){
            res = dao.updateByColumnList(criteria,bean,valueList);
        }else {
            List<String>[] lists = CommonUtil.splitList(valueList, MAX_BATCH_COUNT);
            LOG.debug("依据id批量更新 拆分出====》"+lists.length+"个任务数");
            for (List<String> list : lists) {
                int i = this.updateByColumnList(criteria,bean,list);
                res = res + i;
            }
        }
        LOG.debug("依据列批量更新了====》"+res+"条记录  列====》"+criteria.getWhereInKey()+" 值====》"+valueList);
        return res;
    }

    @Override
    public List<Bean> selectAll() {
        long ls = System.currentTimeMillis();
        List<Bean> beans = dao.selectAll();
        long es = System.currentTimeMillis();
        LOG.debug("selectAll time"+(es-ls)+"/ms");
        return beans;
    }

    @Override
    public boolean isExist(Criteria criteria, DTO bean) {
        long ls = System.currentTimeMillis();
        LOG.debug("查询条件====》"+criteria+" bean====》"+bean);
        boolean exist = dao.isExist(criteria, bean);
        long es = System.currentTimeMillis();
        LOG.debug("isExist time"+(es-ls)+"/ms");
        return exist;
    }

    @Override
    public Bean selectById(Criteria criteria, String id) {
        long ls = System.currentTimeMillis();
        LOG.debug("查询条件====》"+criteria+" id====》"+id);
        Bean bean = dao.selectById(criteria, id);
        long es = System.currentTimeMillis();
        LOG.debug("selectById time"+(es-ls)+"/ms");
        return bean;
    }

    @Override
    public List<Bean> selectByIdList(Criteria criteria, List<String> idList) {
        if(idList.size() <= 0) return Collections.EMPTY_LIST;
        long ls = System.currentTimeMillis();
        LOG.debug("查询条件====》"+criteria+" idList====》"+idList);
        List<Bean> beans = dao.selectByIdList(criteria, idList);
        long es = System.currentTimeMillis();
        LOG.debug("selectByIdList time"+(es-ls)+"/ms");
        return beans;
    }

    @Override
    public List<Bean> selectByColumn(Criteria criteria, String columnValue) {
        long ls = System.currentTimeMillis();
        LOG.debug("查询条件====》"+criteria+" columnValue====》"+columnValue);
        List<Bean> beans = dao.selectByColumn(criteria, columnValue);
        long es = System.currentTimeMillis();
        LOG.debug("selectByColumn time"+(es-ls)+"/ms");
        return beans;
    }

    @Override
    public List<Bean> selectByColumnList(Criteria criteria, List<String> valueList) {
        if(valueList.size() <= 0) return Collections.EMPTY_LIST;
        long ls = System.currentTimeMillis();
        LOG.debug("查询条件====》"+criteria+" valueList====》"+valueList);
        List<Bean> beans = dao.selectByColumnList(criteria, valueList);
        long es = System.currentTimeMillis();
        LOG.debug("selectByColumn time"+(es-ls)+"/ms");
        return beans;
    }

    @Override
    public Bean selectOne(Criteria criteria, DTO bean) {
        long ls = System.currentTimeMillis();
        LOG.debug("查询条件====》"+criteria+" bean====》"+bean);
        Bean one = dao.selectOne(criteria, bean);
        long es = System.currentTimeMillis();
        LOG.debug("selectOne time"+(es-ls)+"/ms");
        return one;
    }

    @Override
    public List<Bean> selectList(Criteria criteria, DTO bean) {
        long ls = System.currentTimeMillis();
        LOG.debug("查询条件====》"+criteria+" bean====》"+bean);
        List<Bean> beans = dao.selectList(criteria, bean);
        long es = System.currentTimeMillis();
        LOG.debug("selectList time"+(es-ls)+"/ms");
        return beans;
    }

    @Override
    public long selectListCount(Criteria criteria, DTO bean) {
        long ls = System.currentTimeMillis();
        LOG.debug("查询条件====》"+criteria+" bean====》"+bean);
        long l = dao.selectListCount(criteria, bean);
        long es = System.currentTimeMillis();
        LOG.debug("selectListCount time"+(es-ls)+"/ms");
        return l;
    }

    /**
     * 获取Bean上的使用主键策略的注解
     * @param bean
     * @return
     */
    private int getAnnotationType(Bean bean){
        PrimaryKey key = bean.getClass().getAnnotation(PrimaryKey.class);
        int defaultType = 0;
        if(key == null)return defaultType;
        if(key.value() == PrimaryKeyTypeEnum.UUID){
            return 0;
        }
        if(key.value() == PrimaryKeyTypeEnum.AUTO){
            return 1;
        }
        if(key.value() == PrimaryKeyTypeEnum.SEQUENCE){
            return defaultType;
        }
        return 2;
    }

    /**
     * 设置主键
     * @param bean
     * @param type
     */
    private void setPrimaryId(Bean bean,int type){
        String value;
        switch (type){
            case 0:
                value = java.util.UUID.randomUUID().toString();
                break;
            case 2: //使用Sequence
            default:
                value = sequence.nextId()+"";
                break;
        }
        if(value == null)return;
        setFieldValueHelp(bean,"id",value);
    }

    /**
     * 反射设置值帮助
     * @param entity
     * @param field
     * @param fieldValue
     */
    private void setFieldValueHelp(Object entity,String field,Object... fieldValue){
        try {
            ReflectUtil.setFieldValue(entity,null,field,fieldValue);
        } catch (InvocationTargetException e) {
            LOG.error(e);
        } catch (IllegalAccessException e) {
            LOG.error(e);
        } catch (NoSuchMethodException e) {
            LOG.error(e);
        }
    }

    /**
     * 反射获取值帮助
     * @param bean
     * @param id
     * @param stringClass
     * @return
     */
    private String getFieldValueHelp(Bean bean, String id, Class<String> stringClass){
        try {
            return ReflectUtil.getFieldValue(bean,"id",String.class);
        } catch (NoSuchMethodException e) {
            LOG.error(e);
        } catch (InvocationTargetException e) {
            LOG.error(e);
        } catch (IllegalAccessException e) {
            LOG.error(e);
        }
        return null;
    }

}
