package com.syl.framework.common.enums;

/**
 * @author syl
 * @create 2018-09-02 19:23
 **/
public enum PrimaryKeyTypeEnum {
    AUTO,
    UUID,
    SEQUENCE
}
