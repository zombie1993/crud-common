package com.syl.framework.common.enums;

/**
 * 数据库类型枚举
 *
 * @author syl
 * @create 2018-03-27 21:12
 **/
public enum DbTypeEnum {
    MYSQL("com.syl.framework.common.limit.impl.MySqlCompute"),
    SQL_SERVER("com.syl.framework.common.limit.impl.SqlServerCompute"),
    ORACLE("com.syl.framework.common.limit.impl.OracleCompute"),
    POSTGRE_SQL("com.syl.framework.common.limit.impl.PostgreSqlCompute"),
    SQLITE("com.syl.framework.common.limit.impl.SqliteCompute");
    private String className;

    DbTypeEnum(String name) {
        this.className= name;
    }

    public String getClassName() {
        return className;
    }

}
