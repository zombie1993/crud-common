package com.syl.framework.common.limit.impl;

import com.syl.framework.common.Criteria;
import com.syl.framework.common.limit.PagingCompute;

/**
 *  sql server 分页参数计算
 * @author syl
 * @create 2018-03-28 21:08
 **/
public class SqlServerCompute implements PagingCompute {

    public void compute(Criteria queryCondition) {
        Integer pageCur = queryCondition.getPageCur();
        Integer pageRow = queryCondition.getPageRow();
        /**
         * offset 0 row 10  1 page
         * offset 10 row 20 2 page
         */
        queryCondition.setOffset((pageCur-1)*pageRow);
        queryCondition.setRow(pageRow * pageCur);
    }

}
