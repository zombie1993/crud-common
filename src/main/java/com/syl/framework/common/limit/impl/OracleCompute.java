package com.syl.framework.common.limit.impl;

import com.syl.framework.common.Criteria;
import com.syl.framework.common.limit.PagingCompute;

/**
 * Oracle 分页参数计算
 *
 * @author syl
 * @create 2018-05-01 19:32
 **/
public class OracleCompute implements PagingCompute{

    @Override
    public void compute(Criteria queryCondition) {
        Integer pageCur = queryCondition.getPageCur();
        Integer pageRow = queryCondition.getPageRow();
        /**
         * offset 0 row 10  1 page
         * offset 10 row 20 2 page
         */
        queryCondition.setOffset((pageCur-1)*pageRow);
        queryCondition.setRow(pageRow * pageCur);
    }

}
