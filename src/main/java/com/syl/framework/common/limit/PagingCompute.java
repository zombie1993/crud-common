package com.syl.framework.common.limit;

import com.syl.framework.common.Criteria;

/**
 * 分页计算基础接口
 *
 * @author syl
 * @create 2018-03-27 20:47
 **/
public interface PagingCompute {

    /**
     * 实际数据库层面分页计算
     * @param queryCondition 查询条件
     */
     void compute(Criteria queryCondition);

}
