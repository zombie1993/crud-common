package com.syl.framework.common.limit.impl;

import com.syl.framework.common.Criteria;
import com.syl.framework.common.limit.PagingCompute;

/**
 * Sqlite 分页参数计算
 *
 * @author syl
 * @create 2018-05-01 19:49
 **/
public class SqliteCompute implements PagingCompute {
    @Override
    public void compute(Criteria queryCondition) {
        Integer pageCur = queryCondition.getPageCur();
        Integer pageRow = queryCondition.getPageRow();
        /**
         * row 10 offset 0 offset代表从第几条记录 之后开始查询，row表明查询多少条结果
         */
        queryCondition.setRow(pageRow);
        queryCondition.setOffset((pageCur-1)*pageRow);
    }

}
