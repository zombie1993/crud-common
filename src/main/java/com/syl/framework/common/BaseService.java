package com.syl.framework.common;

import java.util.List;

/**
 * 基础service 接口
 *
 * @author syl
 * @create 2018-03-17 22:10
 **/
public interface BaseService<Bean,DTO> {

    /**
     * 保存一条记录
     * @param bean
     * @return
     */
    String save(Bean bean);

    /**
     * 批量保存记录
     * @param beanList
     * @return
     */
    List<String> saveList(List<Bean> beanList);

    /**
     *  根据id删除记录
     *  @param id id
     */
    int deleteById(String id);

    /**
     *  根据id批量删除数据
     *  @param idList idList
     */
    int deleteByIdList(List<String> idList);

    /**
     *  根据指定列删除记录
     *  @param criteria 支持whereInKey参数 必需
     *  @param columnValue columnValue
     */
    int deleteByColumn(Criteria criteria,String columnValue);

    /**
     *  根据指定列批量删除数据
     *  @param criteria 支持whereInKey参数 必需
     *  @param valueList valueList
     */
    int deleteByColumnList(Criteria criteria, List<String> valueList);

    /**
     *  根据id更新记录
     *  @param bean set value bean
     *  @param id   id value
     */
    int updateById(Bean bean, String id);

    /**
     *  根据id列表批量更新数据
     *  仅限更新值为一样的情况
     *  @param bean set value bean
     *  @param idList idList
     */
    int updateByIdList(Bean bean, List<String> idList);

    /**
     *  根据指定列更新记录
     *  @param criteria 支持whereInKey参数 必需
     *  @param bean set value bean
     *  @param columnValue columnValue
     */
    int updateByColumn(Criteria criteria, Bean bean,String columnValue);

    /**
     *  根据指定列批量更新数据
     *  仅限更新值为一样的情况
     *
     *  @param criteria 支持whereInKey参数 必需
     *  @param bean set value bean
     *  @param valueList valueList
     */
    int updateByColumnList(Criteria criteria, Bean bean, List<String> valueList);

    /**
     *  查询所有记录
     */
    List<Bean> selectAll();

    /**
     *  依据条件查询是否存在记录
     *  @param criteria criteria 只支持like参数
     *  @param bean bean
     */
    boolean isExist(Criteria criteria, DTO bean);

    /**
     *  根据id查询记录
     *  @param criteria criteria 只支持query参数
     *  @param id id
     */
    Bean selectById(Criteria criteria,String id);

    /**
     * 根据id列表查询记录
     * @param criteria criteria 只支持query参数
     * @param idList idList
     * @return
     */
    List<Bean> selectByIdList(Criteria criteria,List<String> idList);

    /**
     *  根据指定列查询记录
     *  @param criteria criteria whereInKey必填
     *  @param columnValue columnValue 指定列的值
     */
    List<Bean> selectByColumn(Criteria criteria,String columnValue);

    /**
     * 根据指定列查询记录列表
     * @param criteria criteria whereInKey必填
     * @param valueList valueList 多个指定列的值
     * @return
     */
    List<Bean> selectByColumnList(Criteria criteria, List<String> valueList);

    /**
     *  依据条件查询一条记录
     *  @param criteria criteria 支持除whereInKey外所有参数
     *  @param bean bean
     */
    Bean selectOne(Criteria criteria, DTO bean);

    /**
     *  依据条件查询记录列表
     *  @param criteria criteria 支持除whereInKey外所有参数
     *  @param bean bean
     */
    List<Bean> selectList(Criteria criteria, DTO bean);

    /**
     *  依据条件查询记录数量
     *  @param criteria criteria 支持除whereInKey外所有参数
     *  @param bean bean
     */
    long selectListCount(Criteria criteria, DTO bean);

}
