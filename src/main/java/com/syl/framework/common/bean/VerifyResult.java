package com.syl.framework.common.bean;

/**
 * 验证结果类
 *
 * @author syl
 * @create 2018-03-17 21:51
 **/
public class VerifyResult {
    private String field;
    private String message;

    public String getField() {
        return field;
    }

    public VerifyResult setField(String field) {
        this.field = field;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public VerifyResult setMessage(String message) {
        this.message = message;
        return this;
    }
}
