package com.syl.framework.common.bean;

import java.util.List;

/**
 * 页面请求包装类
 *
 * @author syl
 * @create 2018-03-17 20:41
 **/
public class Form<Vo> {
    /**
     * 当前页
     */
    private int pageCur;
    /**
     * 每页显示多少
     */
    private int pageSize;
    /**
     * 提交数据
     */
    private Vo vo;
    /**
     *  排序
     */
    private List<String> orders;

    public int getPageCur() {
        return pageCur;
    }

    public int getPageSize() {
        return pageSize;
    }

    public Vo getVo() {
        return vo;
    }

    public List<String> getOrders() {
        return orders;
    }
}
