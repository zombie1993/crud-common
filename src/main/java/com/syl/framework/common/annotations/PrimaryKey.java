package com.syl.framework.common.annotations;

import com.syl.framework.common.enums.PrimaryKeyTypeEnum;

import java.lang.annotation.*;

/**
 * @author syl
 * @create 2018-09-02 19:22
 **/
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PrimaryKey {

    PrimaryKeyTypeEnum value() default PrimaryKeyTypeEnum.UUID;
}
